import numpy as np
import matplotlib.pyplot as plt


import sys
idd=17
#idd+=1

if len(sys.argv)>1:
  idd=int(sys.argv[1])


import json
with open("../map.json","r") as f:
  mp=json.loads(f.read())

amp={mp[key]-1:key for key in mp.keys()}

# print(amp)

# exit()



f=np.load("images/generated_at_00080.npz")

bq=f["q"]

# print(q.shape)


def draw(A,X):
  phi=np.arange(0,2*np.pi,np.pi*2/len(X))
  x=np.cos(phi)
  y=np.sin(phi)
  #plt.axes("off")

  fig=plt.figure(figsize=(5,5))
  ax=plt.Axes(fig,[0.,0.,1.,1.],xlim=(-1.2,1.2),ylim=(-1.2,1.2))
  ax.clear()
  plt.axis("off")
  ax.set_axis_off()
  fig.add_axes(ax)



  plt.plot(x,y,"o")

  for i in range(len(A)):
    for j in range(len(A)):
      if A[i,j]>0.5:plt.plot([x[i],x[j]],[y[i],y[j]],color="black",alpha=0.5)

  for xx,yy,tex,aphi in zip(x,y,X,phi):
    plt.text(xx,yy,tex,rotation=0*((aphi+45)*180/np.pi)%360,ha="center",va="center",bbox=dict(boxstyle="round",ec=None,fc=(1.0,1.0,1.0)),size=12)

  plt.xlim([-1.5,1.5])
  plt.ylim([-1.5,1.5])




def isconn(A):
  AA=np.array(A)
  for i in range(len(A)):
    AA[i]=1.0
  #print("A",A)
  B= np.linalg.matrix_power(AA,10)
  #print("B",B)
  C=np.prod(B)
  #print("C",C)

  return C!=0.0



def visid(idd):
  #print("idd",idd)
  q=bq[idd]

  # print(q.shape)


  A=q[:9,:9]
  X=q[:,9:]

  flatA=np.abs(A).flatten()
  flatA.sort()

  rouval=flatA[-10]+0.01


  # print(flatA)
  # print(maxdex)

  # exit()

  # print(A.shape,X.shape)


  def round(x):
    if x<rouval:return 0
    return 1

  A=np.array([[round(aaa) for aaa in aa] for aa in A])

  A=A+np.transpose(A)
  A=np.array([[round(aaa) for aaa in aa] for aa in A])


  X=np.array([xx for aa,xx in zip(A,X) if np.sum(aa)>0])

  A=np.array([aa for aa in A if np.sum(aa)>0])
  A=np.transpose(A)
  A=np.array([aa for aa in A if np.sum(aa)>0])
  A=np.transpose(A)

  if not isconn(A):return False#print("!",False)

  if len(A)==0:return False
  #print(f"-----{idd}-----")


  XX=[]

  for i in range(len(A)):
    ii=np.argmax(np.abs(X[i]))
    # print(amp[ii],"*",np.abs(X[i][ii]))
    #print(amp[ii])
    XX.append(amp[ii])
    if XX[-1]=="egg":return False



  draw(X=XX,A=A)
  return True

  # print(A)
  # print(X.shape)


  #print(X[0])
  
  #print("----------")

if __name__=="__main__":
  print(visid(idd))

  plt.show()




