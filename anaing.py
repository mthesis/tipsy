import numpy as np
import matplotlib.pyplot as plt
import json

minuse=3

f=np.load("data.npz",allow_pickle=True)
i=f["i"]
p=f["p"]
m=f["m"]

with open("map.json","r") as f:
  mp=json.loads(f.read())

for ii in range(len(i)):
  for iii in range(len(i[ii])):
    i[ii][iii]=mp[i[ii][iii]]


rel=[]
for ii in i:
  for iii in ii:
    rel.append(iii)

# print(len(rel))
# print(len(list(set(rel))))
print("loaded",len(i),"recipes")


def histn(q):
  ret={}
  for qq in q:
    if qq in ret.keys():
      ret[qq]+=1
    else:
      ret[qq]=1
  return ret

hn=histn(rel)

# with open("map.json","w") as f:
  # f.write(json.dumps({key:key for key in hn.keys()},indent=2))



nn=[hn[key] for key in hn.keys()]

mats=[key for key in hn.keys() if hn[key]>minuse]

print("minuse is",minuse)
print(len(mats),"materials")
print("of",len(hn.keys()))


valides=[]
valip=[]
valim=[]
for ii,pp,mm in zip(i,p,m):
  vali=len([1 for iii in ii if  not iii in mats])==0
  # print(vali,ii,[1 for iii in ii if  not iii in mats])
  # if np.random.randint(10)<3:exit()
  if not vali:continue
  valides.append(ii)
  valip.append(pp)
  valim.append(mm)


print(len(valides),"are valid")

np.savez_compressed("data_1",i=valides,p=valip,m=valim)


exit()



plt.hist(nn,bins=20)

plt.show()




