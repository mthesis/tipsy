import numpy as np
import matplotlib.pyplot as plt

from os import listdir

from draw import *


fns=np.sort(["images/"+q for q in listdir("images/")])


print(fns)


fn=fns[-1]
f=np.load(fn)

q=f["q"]

dx,dy=5,5
fix,axs=plt.subplots(dx,dy)

plt.axis("off")

i=0
for ix in range(dx):
  for iy in range(dy):

    draw(q[i],ax=axs[ix,iy])

    #axs[ix,iy].axis("off")

    i+=1
plt.show()


