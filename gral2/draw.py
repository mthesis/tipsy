import numpy as np
import matplotlib.pyplot as plt 

def draw(d,ax=None):
  if ax is None:ax=plt
  x,y=d[:,0],d[:,1]
  
  dt=np.zeros((8,8),dtype="float")
  
  for i in range(8):
    for j in range(8):
      dt[i,j]=(x[i]-x[j])**2+(y[i]-y[j])**2
      if i==j:dt[i,j]=10000
  
  for i in range(8):
    i2=np.argmin(dt[i])
    dt[i2,i]=1000
    ax.plot([x[i],x[i2]],[y[i],y[i2]],color="black",alpha=0.1)
  
  
  ax.plot(x,y,"o",color="black")
  

if __name__=="__main__":
  f=np.load("data.npz")
  d=f["d"]
  i=f["i"]
  
  
  idd=15+2
  
  draw(d[idd])
  
  print(i[idd])
  
  plt.show()




