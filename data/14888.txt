{
  "drinks": [
    {
      "idDrink": "14888",
      "strDrink": "Zinger",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Soft Drink / Soda",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Highball glass",
      "strInstructions": "Get a shot glass and pour in three shots of the schnapps. Do the same with the Surge Cola. Then down it like Scheetz would.",
      "strInstructionsES": null,
      "strInstructionsDE": "Holen Sie sich ein Schnapsglas und geben Sie drei Sch\u00fcsse des Schnapses hinein. Das Gleiche gilt f\u00fcr den Schwall Cola.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/iixv4l1485620014.jpg",
      "strIngredient1": "Peachtree schnapps",
      "strIngredient2": "Surge",
      "strIngredient3": null,
      "strIngredient4": null,
      "strIngredient5": null,
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "4 shots ",
      "strMeasure2": "4 shots ",
      "strMeasure3": null,
      "strMeasure4": null,
      "strMeasure5": null,
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2017-01-28 16:13:34"
    }
  ]
}