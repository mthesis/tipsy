{
  "drinks": [
    {
      "idDrink": "17824",
      "strDrink": "The Laverstoke",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Cocktail",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Balloon Glass",
      "strInstructions": "1) Squeeze two lime wedges into a balloon glass then add the cordial, Bombay Sapphire and MARTINI Rosso Vermouth, swirl to mix.\r\n\r\n2) Fully fill the glass with cubed ice and stir to chill.\r\n\r\n3) Top with Fever-Tree Ginger Ale and gently stir again to combine.\r\n\r\n4) Garnish with a snapped ginger slice and an awoken mint sprig.",
      "strInstructionsES": null,
      "strInstructionsDE": "Zwei Limettenkeile in ein Ballonglas dr\u00fccken und dann die Herznote, Bombay Sapphire und MARTINI Rosso Wermut hinzuf\u00fcgen, verr\u00fchren. F\u00fcllen Sie das Glas vollst\u00e4ndig mit Eisw\u00fcrfeln und r\u00fchren Sie es um, um es abzuk\u00fchlen. Mit dem Fever-Tree Ginger Ale auff\u00fcllen und zum Mischen wieder leicht umr\u00fchren. Mit einer ge\u00f6ffneten Ingwerscheibe und einem Minzzweig garnieren.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/6xfj5t1517748412.jpg",
      "strIngredient1": "Gin",
      "strIngredient2": "Elderflower cordial",
      "strIngredient3": "Rosso Vermouth",
      "strIngredient4": "Tonic Water",
      "strIngredient5": "Lime",
      "strIngredient6": "Ginger",
      "strIngredient7": "Mint",
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "50 ml",
      "strMeasure2": "15 ml",
      "strMeasure3": "15 ml",
      "strMeasure4": "75 ml",
      "strMeasure5": "2 Wedges",
      "strMeasure6": "1 Slice",
      "strMeasure7": "1 Large Sprig",
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2018-02-04 12:46:52"
    }
  ]
}