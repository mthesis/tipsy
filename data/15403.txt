{
  "drinks": [
    {
      "idDrink": "15403",
      "strDrink": "Vodka Russian",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Ordinary Drink",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Collins Glass",
      "strInstructions": "Mix it as a ordinary drink .",
      "strInstructionsES": null,
      "strInstructionsDE": "Mischen Sie es wie ein gew\u00f6hnliches Getr\u00e4nk.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/rpttur1454515129.jpg",
      "strIngredient1": "Vodka",
      "strIngredient2": "Schweppes Russchian",
      "strIngredient3": null,
      "strIngredient4": null,
      "strIngredient5": null,
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "2 oz ",
      "strMeasure2": null,
      "strMeasure3": null,
      "strMeasure4": null,
      "strMeasure5": null,
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2016-02-03 15:58:49"
    }
  ]
}