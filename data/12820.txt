{
  "drinks": [
    {
      "idDrink": "12820",
      "strDrink": "Irish Cream",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Homemade Liqueur",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Irish coffee cup",
      "strInstructions": "Mix scotch and milk. Add half-and-half. Add rest.",
      "strInstructionsES": null,
      "strInstructionsDE": "Halb Scotch und halb Milch mischen. Rest hinzuf\u00fcgen.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/90etyl1504884699.jpg",
      "strIngredient1": "Scotch",
      "strIngredient2": "Half-and-half",
      "strIngredient3": "Condensed milk",
      "strIngredient4": "Coconut syrup",
      "strIngredient5": "Chocolate syrup",
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "1 cup ",
      "strMeasure2": "1 1/4 cup ",
      "strMeasure3": "1 can sweetened ",
      "strMeasure4": "3 drops ",
      "strMeasure5": "1 tblsp ",
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2017-09-08 16:31:39"
    }
  ]
}