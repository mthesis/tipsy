{
  "drinks": [
    {
      "idDrink": "13198",
      "strDrink": "Quick F**K",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Shot",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Shot glass",
      "strInstructions": "In a shot glass add 1/3 Kahlua first. Then 1/3 Miduri, topping it off with a 1/3 bailey's irish cream",
      "strInstructionsES": null,
      "strInstructionsDE": "In einem Schnapsglas zuerst 1/3 Kahlua geben. Dann 1/3 Miduri, erg\u00e4nzt durch 1/3 Bailey's irish Cream.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/wvtwpp1478963454.jpg",
      "strIngredient1": "Kahlua",
      "strIngredient2": "Midori melon liqueur",
      "strIngredient3": "Baileys irish cream",
      "strIngredient4": null,
      "strIngredient5": null,
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "1 part ",
      "strMeasure2": "1 part ",
      "strMeasure3": "1 part ",
      "strMeasure4": null,
      "strMeasure5": null,
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2016-11-12 15:10:54"
    }
  ]
}