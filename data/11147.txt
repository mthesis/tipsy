{
  "drinks": [
    {
      "idDrink": "11147",
      "strDrink": "Bourbon Sour",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Ordinary Drink",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Whiskey sour glass",
      "strInstructions": "In a shaker half-filled with ice cubes, combine the bourbon, lemon juice, and sugar. Shake well. Strain into a whiskey sour glass, garnish with the orange slice and cherry.",
      "strInstructionsES": null,
      "strInstructionsDE": "In einem Shaker, der halb mit Eisw\u00fcrfeln gef\u00fcllt ist, Bourbon, Zitronensaft und Zucker vermengen. Gut sch\u00fctteln. In ein Whiskey Sour Glas abseihen, mit der Orangenscheibe und der Kirsche garnieren.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/dms3io1504366318.jpg",
      "strIngredient1": "Bourbon",
      "strIngredient2": "Lemon juice",
      "strIngredient3": "Sugar",
      "strIngredient4": "Orange",
      "strIngredient5": "Maraschino cherry",
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "2 oz ",
      "strMeasure2": "1 oz ",
      "strMeasure3": "1/2 tsp superfine ",
      "strMeasure4": "1 ",
      "strMeasure5": "1 ",
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2017-09-02 16:31:58"
    }
  ]
}