{
  "drinks": [
    {
      "idDrink": "12130",
      "strDrink": "Scooter",
      "strDrinkAlternate": null,
      "strDrinkES": null,
      "strDrinkDE": null,
      "strDrinkFR": null,
      "strDrinkZH-HANS": null,
      "strDrinkZH-HANT": null,
      "strTags": null,
      "strVideo": null,
      "strCategory": "Ordinary Drink",
      "strIBA": null,
      "strAlcoholic": "Alcoholic",
      "strGlass": "Cocktail glass",
      "strInstructions": "Shake all ingredients well with cracked ice, strain into a cocktail glass, and serve.",
      "strInstructionsES": null,
      "strInstructionsDE": "Alle Zutaten gut mit gebrochenem Eis sch\u00fctteln, in ein Cocktailglas abseihen und servieren.",
      "strInstructionsFR": null,
      "strInstructionsZH-HANS": null,
      "strInstructionsZH-HANT": null,
      "strDrinkThumb": "https://www.thecocktaildb.com/images/media/drink/twuptu1483388307.jpg",
      "strIngredient1": "Brandy",
      "strIngredient2": "Amaretto",
      "strIngredient3": "Light cream",
      "strIngredient4": null,
      "strIngredient5": null,
      "strIngredient6": null,
      "strIngredient7": null,
      "strIngredient8": null,
      "strIngredient9": null,
      "strIngredient10": null,
      "strIngredient11": null,
      "strIngredient12": null,
      "strIngredient13": null,
      "strIngredient14": null,
      "strIngredient15": null,
      "strMeasure1": "1 oz ",
      "strMeasure2": "1 oz ",
      "strMeasure3": "1 oz ",
      "strMeasure4": null,
      "strMeasure5": null,
      "strMeasure6": null,
      "strMeasure7": null,
      "strMeasure8": null,
      "strMeasure9": null,
      "strMeasure10": null,
      "strMeasure11": null,
      "strMeasure12": null,
      "strMeasure13": null,
      "strMeasure14": null,
      "strMeasure15": null,
      "strCreativeCommonsConfirmed": "No",
      "dateModified": "2017-01-02 20:18:27"
    }
  ]
}