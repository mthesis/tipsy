import numpy as np


f=np.load("data_1.npz",allow_pickle=True)

p=f["p"]
m=f["m"]
i=f["i"]

def isnum(q):
  try:
    x=float(q)
    return True
  except:
    return False

def notnum(x):
  if x is None:return ""
  return "".join([q for q in x if not (isnum(q) or q==" ")])


mf=[]
for mm in m:
  for mmm in mm:
    mf.append(mmm)

mfnn=[notnum(q) for q in mf]

print(set(mfnn))



# print(m)