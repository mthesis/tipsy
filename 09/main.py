#stolen from https://github.com/eriklindernoren/Keras-GAN/blob/master/dcgan/dcgan.py




from __future__ import print_function, division


ooc=100


import os

os.system("rm images/g*")

from tensorflow.keras.datasets import mnist
from tensorflow.keras.layers import Input, Dense, Reshape, Flatten, Dropout
from tensorflow.keras.layers import BatchNormalization, Activation, ZeroPadding2D
from tensorflow.keras.models import Sequential, Model
from tensorflow.keras.optimizers import Adam
from tensorflow.keras.losses import mse

import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K


import matplotlib.pyplot as plt

import sys

import numpy as np

from grapa.functionals import *
from grapa.constants import *
from grapa.layers import *


import json

def exporth(history):
  with open("history.json","w") as f:
    f.write(json.dumps(history,indent=2))

def oneoffloss(y_true,y_pred):
  
  loss=K.mean(np.abs(y_pred-0.8))


  return K.sum(y_true)*loss

class DCGAN():

    def dcompile1(self):
        self.discriminator.compile(loss='binary_crossentropy',
            optimizer=self.optimizer,
            metrics=['accuracy'])
    def dcompile2(self):
        self.discriminator.compile(loss="mse",
            optimizer=self.optimizer,
            metrics=[])
      

    def __init__(self,modus="classic"):
        # Input shape
        self.img_rows = 28
        self.img_cols = 28
        self.channels = 1
        self.img_shape = (self.img_rows, self.img_cols, self.channels)
        self.latent_dim = 100

        self.gs=9
        self.param=47
        self.baseparam=50

        self.modus=modus

        lr=0.0002
        if modus=="oneoff":lr=0.01


        self.optimizer = Adam(lr, 0.5)

        # Build and compile the discriminator
        self.discriminator = self.build_discriminator()

        if modus=="classic":self.dcompile1()
        if modus=="oneoff":self.dcompile2()


        # Build the generator
        self.generator = self.build_generator()

        # The generator takes noise as input and generates imgs
        z = Input(shape=(self.latent_dim,))
        img = self.generator(z)

        # For the combined model we will only train the generator
        self.discriminator.trainable = False

        # The discriminator takes generated images as input and determines validity
        valid = self.discriminator(img)

        # The combined model  (stacked generator and discriminator)
        # Trains the generator to fool the discriminator
        self.combined = Model(z, valid)
        self.combined.compile(loss='binary_crossentropy', optimizer=self.optimizer)

    def build_generator(self):
        #takes (batch_size,latent_dim)
        #outputs (batch_size,8,2) 

        inn=Input(shape=(self.latent_dim,))
        base=Dense(self.baseparam,activation="relu")(inn)

        base=Reshape((1,self.baseparam))(base)


        g=grap(state(gs=1,param=self.baseparam))
        g.X=base

        m=getm()

        #g=multidense(g,m,[self.baseparam,16,16,16])

        #not actually needed i guess
        g.A=gcomfullyconnected(gs=g.s.gs,param=g.s.param)([g.X])

        #g=gll(g,m,k=1)


        #g=divpar(g,m=m,c=2)


        #g.X=Reshape((g.s.gs*g.s.param,))(g.X)
        #g.s.param*=g.s.gs
        #g.s.gs=1
        #g=multidense(g,m,[16])
        #g.X=Reshape((8,2))(g.X)



        amode2="prod"


        g=divpar(g,c=3,m=m,amode2=amode2)
        
        g=gnl(g,m)
        g=gnl(g,m)
        g=gnl(g,m)

        g=divpar(g,c=3,m=m,amode2=amode2)
     
        g=gnl(g,m)
        g=gnl(g,m)
        g=gnl(g,m)

        g=remparam(g,self.param)

        ret=ghealparam(gs=g.s.gs,param1=g.s.gs,param2=g.s.param)([g.A,g.X])

        model= Model(inn,ret,name="generator")

        plot_model(model,to_file="generator.png",show_shapes=True)

        return model

    def build_discriminator(self):
        #takes (batch_size,8,2)
        #should return 1d Dense        

        inn=Input(shape=(self.gs,self.gs+self.param))
        g=grap(state(gs=self.gs,param=self.param))
        g.A,g.X=gcutparam(gs=self.gs,param1=self.gs,param2=self.gs)([inn])

        m=getm()

        #g=gll(g,m,k=3,free=3)
        g=gnl(g,m)

        #g=gll(g,m,k=4,free=3)
        g=gnl(g,m)
     
         
        #g=gll(g,m,k=2,free=0)
        g=gnl(g,m)

        g.X=gpool(gs=g.s.gs,param=g.s.param,mode="max")([g.X])
        g.s.gs=1
       
        g=multidense(g,m,[8,6,4,2]) 

        #if self.modus=="classic":g.X=Dense(1,activation="softmax")(g.X)
        if self.modus=="classic":g.X=Dense(1,activation="linear")(g.X)
        if not self.modus=="classic":g.X=Dense(1,activation="linear",bias_constraint=tf.keras.constraints.MaxNorm(max_value=0.1))(g.X)


        model= Model(inn,g.X,name="discriminator")

        plot_model(model,to_file="discriminator.png",show_shapes=True)

        return model

    def trainoo(self,epochs,batch_size=128):
        f=np.load("../data_4.npz")
        X_train=np.concatenate((f["a"],f["x"]),axis=-1)
        
        shouldbe=np.ones((batch_size,1))

        for epoch in range(epochs):
            # Select a random half of images
            idx = np.random.randint(0, X_train.shape[0], batch_size)
            imgs = X_train[idx]

            print(epoch,self.discriminator.train_on_batch(imgs,shouldbe))

    def train(self, epochs, batch_size=128, save_interval=50):

        # Load the dataset
        #(X_train, _), (_, _) = mnist.load_data()

        f=np.load("../data_4.npz")
        X_train=np.concatenate((f["a"],f["x"]),axis=-1)


        # Rescale -1 to 1
        #X_train = X_train / 127.5 - 1.
        #X_train = np.expand_dims(X_train, axis=3)

        # Adversarial ground truths
        valid = np.ones((batch_size, 1))
        fake = np.zeros((batch_size, 1))


        history=[]

        for epoch in range(epochs):

            # ---------------------
            #  Train Discriminator
            # ---------------------

            # Select a random half of images
            idx = np.random.randint(0, X_train.shape[0], batch_size)
            imgs = X_train[idx]

            # Sample noise and generate a batch of new images
            noise = np.random.normal(0, 1, (batch_size, self.latent_dim))
            gen_imgs = self.generator.predict(noise)

            # Train the discriminator (real classified as ones and generated as zeros)
            d_loss_real = self.discriminator.train_on_batch(imgs, valid)
            d_loss_fake = self.discriminator.train_on_batch(gen_imgs, fake)
            d_loss = 0.5 * np.add(d_loss_real, d_loss_fake)

            #pp=self.discriminator.predict(imgs)
            #print(pp)
            #exit()


            # ---------------------
            #  Train Generator
            # ---------------------

            # Train the generator (wants discriminator to mistake images as real)
            g_loss = self.combined.train_on_batch(noise, valid)

            # Plot the progress
            print ("%d [D loss: %f, acc.: %.2f%%] [G loss: %f]" % (epoch, d_loss[0], 100*d_loss[1], g_loss))

            history.append({"epoch":int(epoch),"D loss":float(d_loss[0]),"D acc":float(d_loss[1]),"g loss":float(g_loss)})

            exporth(history)

            # If at save interval => save generated image samples
            if epoch % save_interval == 0:
                self.save_imgs(epoch)
            
            #if epoch==0:self.decompile1()

    def save_imgs(self, epoch):
        r, c = 50, 50
        noise = np.random.normal(0, 1, (r * c, self.latent_dim))
        gen_imgs = self.generator.predict(noise)

        np.savez_compressed("images/generated_at_%05d" % epoch,noise=noise,q=gen_imgs)

    def exportdisc(self):
      #print(dir(self.discriminator))

      #exit()

      return self.discriminator.get_weights()



    def importdisc(self,q):
      self.discriminator.set_weights(q)


if __name__ == '__main__':
    if ooc>0:dcgan = DCGAN("oneoff")

    if ooc>0:dcgan.trainoo(epochs=ooc)


    dc2=DCGAN("classic")

    if ooc>0:dc2.importdisc(dcgan.exportdisc())

    dc2.train(epochs=401, batch_size=32, save_interval=20)
