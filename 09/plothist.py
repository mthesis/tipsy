import numpy as np
import matplotlib.pyplot as plt
import json


with open("history.json","r") as f:
  q=json.loads(f.read())

Dl=[qq["D loss"] for qq in q]
gl=[qq["g loss"] for qq in q]
ep=[qq["epoch"] for qq in q]



plt.plot(ep,Dl,label="discriminator")
plt.plot(ep,gl,label="generator")


plt.legend()
plt.show()

