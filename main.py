import random
import sys
import codecs
import requests
import os
import json



vers=sys.version
vers=vers[:vers.find(".")]
vers=float(vers)

if vers<3:
  import urllib
  import urllib2
else:
  # import urllib.request as u
  import urllib.parse
  import urllib.request


link="https://www.thecocktaildb.com/api/json/v1/1/random.php"




def loadsite(url,values=None):
  if not values is None:
    if vers<3:
      data = urllib.urlencode(values)
      req = urllib2.Request(url, data)
      response = urllib2.urlopen(req)
      the_page = response.read()
      return the_page.decode("utf-8")
    else:
      data = urllib.parse.urlencode(values,{})
      # print(data)
      req = urllib.request.Request(url, data.encode("utf-8"))
      response = urllib.request.urlopen(req)
      the_page = response.read()
      return the_page.decode("utf-8")
  else:
    if vers<3:
      req = urllib2.Request(url)
      response = urllib2.urlopen(req)
      the_page = response.read()
      return the_page.decode("utf-8")
    else:
      req = urllib.request.Request(url)
      response = urllib.request.urlopen(req)
      the_page = response.read()
      return the_page.decode("utf-8")


def runone():
  q=loadsite(link)
  qj=json.loads(q)
  idd=qj["drinks"][0]["idDrink"]
  with open("data/"+str(idd)+".txt","w") as f:
    f.write(json.dumps(qj,indent=2))
  print("downloaded",idd)






while True:runone()  












