import numpy as np
import json

fnam="alpha"
import sys
if len(sys.argv)>1:fnam=sys.argv[1]

fnam="h"+fnam+".json"


with open(fnam,"r") as f:
  b=json.loads(f.read())


gl=[[bbb["g loss"] for bbb in bb["h"]] for bb in b]


print(np.max(gl))

