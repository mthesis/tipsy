import numpy as np
import os
import json

import sys
ind="0"
if len(sys.argv)>1:ind=sys.argv[1]


from main import DCGAN as model


histories=[]

def saveh():
  with open(f"h{ind}.json","w") as f:
    f.write(json.dumps(histories,indent=2))


for i in range(1000):

  lr=np.exp(-4-3*np.random.random())

  bat=int(1+np.random.random()*10+np.exp(5*np.random.random()))


  ac=model(lr=lr)
  
  histories.append({"lr":lr,"bat":bat,"h":ac.train(epochs=50,batch_size=bat,save_interval=10000)})
  saveh()





