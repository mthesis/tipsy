import numpy as np
import matplotlib.pyplot as plt

f=np.load("data_3.npz",allow_pickle=True)

x=f["x"]
a=f["a"]

l=[np.array(xx).shape[0] for xx in x]
# print(l)
# plt.hist(l,bins=20)
# plt.show()

s=9

x=[xx for xx,ll in zip(x,l) if ll<=s]
a=[aa for aa,ll in zip(a,l) if ll<=s]


xx=np.array([np.concatenate((ax,np.zeros((s-len(ax),np.array(ax).shape[-1]))),axis=0) for ax in x])
aq=[]
for aa in a:
    la=len(aa)
    if la>s:continue
    ap=np.zeros((s-len(aa),len(aa)))
    aa=np.concatenate((aa,ap),axis=0)
    ap=np.zeros((len(aa),s-la))
    aa=np.concatenate((aa,ap),axis=1)
    aq.append(aa)
aq=np.array(aq)


np.savez_compressed("data_4",x=xx,a=aq)