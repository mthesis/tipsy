import numpy as np
import os
import json


fns=["data/"+q for q in os.listdir("data/")]

def load(q):
  with open(q,"r") as f:
    return json.loads(f.read())

cs=[load(fn)["drinks"][0] for fn in fns]

def simp(c):
  instr=c["strInstructions"]
  ingre=[]
  for i in range(1,16):
    ingre.append(c[f"strIngredient{i}"])
  ingre=[i for i in ingre if not i is None]
  meas=[c[f"strMeasure{i}"] for i in range(1,len(ingre)+1)]
  
  return instr,ingre,meas

def sentencesplit(q):
  return [ac for ac in q.split(".") if len(ac)>0]

def ingresplitone(q):
  return [ac for ac in q.split(" ") if len(ac)>0]
def ingresplit(q):
  return [ingresplitone(ac) for ac in q]

def numi(w):
  ret=[]
  for ww in w:
    for www in ww:
      ret.append(www)
  return list(set(ret))

def work(c):

  instr,ingre,meas=simp(c)
  
  instr=instr.lower()
  ingre=[q.lower() for q in ingre]

  seninstr=sentencesplit(instr)
  q=ingresplit(ingre)

  places=[[] for i in seninstr]

  for i,inst in enumerate(seninstr):
    for j,ing in enumerate(q):
      val=np.sum([1 for i in ing if i in inst])>0
      if val:places[i].append(j)
  if not len(numi(places))==len(ingre):return None,None,None
  return places,ingre,meas

places=[]
ingredients=[]
measurements=[]

fail=0
for i in range(len(cs)):
  w,i,m=work(cs[i])
  if w is None:
    fail+=1
    continue
  places.append(w)
  ingredients.append(i)
  measurements.append(m)


np.savez_compressed("data",p=places,i=ingredients,m=measurements)


  
  
print(fail,len(cs))





