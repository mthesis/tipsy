import numpy as np


f=np.load("data_1.npz",allow_pickle=True)


trafos={"\n":"",
        "shots":"*45",
        "shot":"*45",
        "splashes":"*6",
        "glass":"*100",
        "crushed":"",
        "cans":"*250",
        "Mexican":"",
        "beaten":"",
        "Juice":"",
        "of":"",
        "oz":"*30",
        "tblsp":"*15",
        "measures":"",
        "instant":"",
        "fresh":"",
        "cupskimmed":"*180",
        "cups":"*240",
        "cup":"*240",
        "boiling":"",
        "(seltzerwater)":"",
        "Coarse":"",
        "Bacardi":"",
        "lemon":"",
        "tspsuperfine":"*4",
        "tspgrated":"*4",
        "tsp":"*5",
        "sweet":"",
        "Topupwith":"",
        "\r":"",
        "-":"",
        "hot":"",
        "Topupwith":"",
        "ligthordark":"",
        "cl":"*10",
        "stick":"*120",
        "powdered":"",
        "sweetened":"",
        "cubes":"",
        "twistof":"",
        "Twistof":"",
        "Fresh":"",
        "Chilled":"",
        "ml":"",
        "Bacardi":"",
        "dashes":"*0.92",
        "bottle":"*1000",
        "hot":"",
        "slice":"*0.1",
        "inch":"*16",
        "whole":"",
        "Ground":"",
        "Grated":"",
        "finely":"",
        "chopped":"",
        "dark":"",
        "Chilled":"",
        "jigger":"",
        "cold":"",
        "wedge":"*0.125",
        "garnish":"",
        "Fillwith":"",
        "qt":"*946",
        "white":"",
        "":"",
        "skimmed":"",
        "Joiceof":"",
        "can":"*250",
        "strong":"",
        "black":"",
        "dash":"*0.92",
        "totaste":"",
        "dry":"",
        "whole":"",
        "Thai":"",
        "1 1/2":"1.5",
        "2 1/2":"2.5",
        "twist":"",
        "superfine":"",
        "grated":"",
        "cube":"",
        "light":"",
        "or":"",
        "Twist":"",
        "seltzer":"",
        "water":"",
        "( )":"",
        "()":"",
        "ened":"",
        "to":"",
        "taste":"",
        "Fill":"",
        "with":"",
        "Top":"",
        "up":"",
        "Garnish":"",
        }

def trafo(x):
  if x is None:return -1.0
  for key in trafos.keys():
    x=x.replace(key,trafos[key])
  x=x.strip()
  if len(x)==0:return -1.0
  if x[0]=="*":x="1"+x
  return float(eval(x))



p=f["p"]
m=f["m"]
i=f["i"]

#also clean up p

p=[[ppp for ppp in pp if len(ppp)>0] for pp in p]

print([len(ppp) for ppp in p[0]])




for mm in range(len(m)):
  for mmm in range(len(m[mm])):
    m[mm][mmm]=trafo(m[mm][mmm])

print(i[0])
print(p[0])
print(m[0])



np.savez_compressed("data_2",m=m,p=p,i=i)

